import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './pages/index/index.component';
import { SearchResultsComponent } from './pages/search-results/search-results.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { SuccessReservationComponent } from './components/success-reservation/success-reservation.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { AdminComponent } from './pages/admin/admin.component';
import { AuthGaurdService } from './services/auth-gaurd.service';
import { UserAccountComponent } from './pages/user-account/user-account.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { UserReservationComponent } from './pages/user-reservation/user-reservation.component';
import { GuideAccountComponent } from './pages/guide-account/guide-account.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';


const routes: Routes = [
  { path: '', redirectTo: 'index', pathMatch: 'full' },
  { path: 'index', component: IndexComponent },
  { path: 'search-results', component: SearchResultsComponent },
  { path: 'checkout', component: CheckoutComponent },
  { path: 'success-reservation', component: SuccessReservationComponent },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'admin', component: AdminComponent, canActivate:[AuthGaurdService] },
  { path: 'user-account', component: UserAccountComponent, canActivate:[AuthGaurdService] },
  { path: 'user-account/profile', component: UserProfileComponent, canActivate:[AuthGaurdService] },
  { path: 'user-account/reservation', component: UserReservationComponent, canActivate:[AuthGaurdService] },
  { path: 'guide-account', component: GuideAccountComponent, canActivate:[AuthGaurdService] },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [IndexComponent, SearchResultsComponent, PageNotFoundComponent, LoginComponent, CheckoutComponent,RegisterComponent, AdminComponent, UserAccountComponent, GuideAccountComponent ]
