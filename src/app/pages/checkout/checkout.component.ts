import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { CheckoutService } from '../../services/checkout.service';
import { of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { differenceInDays } from 'date-fns';
import { CreditCard } from '../../data/creditcard';
import { Router } from '@angular/router';
import { indexSearchI } from '../../data/indexSearch';
import { IndexSearchService } from '../../services/indexSearch.service';
import { ValidationService } from '../../services/validation.service';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  checkin;
  checkout;
  guide;
  guidePrice;
  diffDate;
  checkThisIn;
  checkThisOut;
  indexSearch: indexSearchI;
  registerForm: any;
  creditcard = new CreditCard('', '', '', '', '');
  submitted = false;
  buttonDisabled = true;
  reservation;
  contract = false;
  userNote;


  constructor(private checkoutService: CheckoutService, private indexSearchService: IndexSearchService, private formBuilder: FormBuilder, private router: Router) { }

  setGuideOnPage(guide_id) {
    this.checkoutService.getCheckoutGuide(guide_id).pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe(object => {
        this.guide = object;
        this.setGuidePrice();
      });
  }

  setGuidePrice() {
    this.checkoutService.getGuidePrice(this.indexSearch.guide.id, this.indexSearch.place.id).pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe(object => {
        this.guidePrice = object;
        this.checkThisIn = this.indexSearch.checkin.split("-");
        this.checkThisOut = this.indexSearch.checkout.split("-");
        this.diffDate = differenceInDays(
          new Date(this.checkThisOut[2], this.checkThisOut[1], this.checkThisOut[0]),
          new Date(this.checkThisIn[2], this.checkThisIn[1], this.checkThisIn[0])
        ) + 1
        this.guidePrice.guidingPrice = this.guidePrice.guidingPrice * this.diffDate;
        this.guidePrice.drivingPrice = this.guidePrice.drivingPrice * this.diffDate;
      });
  }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);
    this.setGuideOnPage(this.indexSearch.guide);

    this.checkin = this.indexSearch.checkin;
    this.checkout = this.indexSearch.checkout;

    this.registerForm = this.formBuilder.group({
      holder: ['', [Validators.required, ValidationService.holderValidator]],
      month: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2), ValidationService.monthValidator]],
      year: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(2), ValidationService.yearValidator]],
      ccn: ['', [Validators.required, Validators.minLength(16), Validators.maxLength(16)]],
      cvc: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(3)]]
    });
  }

  get f() { return this.registerForm.controls; }
  
  toggleVisibility(e) {
    this.submitted = true;
    this.contract = e.target.checked;

    if (this.contract == true && this.registerForm.invalid == false) {
      this.buttonDisabled = false;
    } else {
      this.buttonDisabled = true;
    }
  }

  onSubmit() {
    this.submitted = true;
    if (this.buttonDisabled == true) {
      return;
    } else if (this.buttonDisabled == false) {
      let total_price = this.guidePrice.guidingPrice + this.guidePrice.drivingPrice;
      this.checkoutService.addReservation(this.guide.id, this.indexSearch.user.id, this.indexSearch.place.id, this.indexSearch.aim.id, total_price, "Ödendi", this.userNote, this.indexSearch.checkin, this.indexSearch.checkout).pipe(
        retry(2),
        catchError(() => {
          return of({});
        })).subscribe(object => {
          this.reservation = object;
          if (this.reservation.reservationStatus == "Success") {
            this.buttonDisabled = true;
            this.navigationSuccess();
          } else if (this.reservation.reservationStatus == "Fail") {
            this.buttonDisabled = false;
          }
        });
    }
  }

  navigationSuccess() {
    this.router.navigate(['success-reservation']);
  }

}
