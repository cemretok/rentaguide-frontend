import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { SearchResultsService } from '../../services/search-results.service';


@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  checkin;
  checkout;

  constructor(private route: ActivatedRoute, private searchResult: SearchResultsService, private router: Router) {}

  ngOnInit() {
  }

}


