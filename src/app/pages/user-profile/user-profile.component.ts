import { Component, OnInit } from '@angular/core';
import { User } from '../../data/user';
import { UserService } from '../..//services/user-profile.service';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit {
  users: User;
  constructor(private userService: UserService) {
    this.userService = userService;
   }

  ngOnInit(): void {
  }

}
