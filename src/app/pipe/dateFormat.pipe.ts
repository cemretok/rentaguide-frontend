import { Pipe, PipeTransform } from '@angular/core';
import { DatePipe } from '@angular/common';

@Pipe({
    name: 'dateFormat',
})
export class DateFormatPipe implements PipeTransform {
    transform(value: string) {
        if(value == null){
            value = "01/01/1989";
        }
        var datePipe = new DatePipe("en-US");
        let day = value.substring(0, 2);
        let month = value.substring(3, 5);
        let year = value.substring(6, 10);

        return day + "/" + month + "/" + year;
    }
}