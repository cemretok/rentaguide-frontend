export class constants {
    public static USER_TYPE_USER: String = "User";
    public static USER_TYPE_GUIDE: String = "Guide";
    public static USER_TYPE_ADMIN: String = "Admin";
}