import { Component, OnInit } from '@angular/core';
import { CountryInputService } from '../../services/country-input.service';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, catchError, retry } from 'rxjs/operators';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-country-input',
  templateUrl: './country-input.component.html',
  styleUrls: ['./country-input.component.css']
})
export class CountryInputComponent implements OnInit {

  model: any;
  place_id = null;
  searchObject = {};
  searchArray = [];
  searchArrayId = [];
  indexSearch: indexSearchI;

  constructor(private countryInputService: CountryInputService, private indexSearchService: IndexSearchService) { }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);
    let tempModal = this.indexSearch.place.country + ", " + this.indexSearch.place.province + ", " + this.indexSearch.place.district;
    
    if (tempModal.length > 4)
      this.model = tempModal
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term === '' ? []
        : this.getPlaces().filter(v => v.toLocaleLowerCase().indexOf(term.toLocaleLowerCase()) > -1).slice(0, 10))
    )

  getPlaces() {
    this.countryInputService.getCountry().pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe(object => {
        this.searchArray = Object.keys(object).map(i => object[i].country + ", " + object[i].province + ", " + object[i].district);
        this.searchArrayId = Object.keys(object).map(i => {
          return {
            "id": object[i].id,
            "name": object[i].country + ", " + object[i].province + ", " + object[i].district
          }
        });
      });
    return this.searchArray;
  }

  dataChanged(data) {
    if (this.searchArrayId.length > 0 && data != null) {
      Object.keys(this.searchArrayId).forEach(i => {
        if (this.searchArrayId[i].name == data) {
          this.place_id = this.searchArrayId[i].id;


          this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);

          let parsedCountry = data.split(", ");

          this.indexSearch.place.id = this.place_id;
          this.indexSearch.place.country = parsedCountry[0];
          this.indexSearch.place.province = parsedCountry[1];
          this.indexSearch.place.district = parsedCountry[2];

          this.indexSearchService.changeMessage(this.indexSearch);
        }
      });
    }
  }
}