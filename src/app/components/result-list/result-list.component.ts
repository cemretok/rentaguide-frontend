import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-result-list',
  templateUrl: './result-list.component.html',
  styleUrls: ['./result-list.component.css']
})
export class SearchResultComponent implements OnInit {

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit() {
    }

}
