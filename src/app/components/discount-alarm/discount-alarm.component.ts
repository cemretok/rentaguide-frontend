import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

interface Alert {
  type: string;
  message: string;
}

const ALERTS: Alert[] = [{
  type: 'success',
  message: 'Sign in and get %10 discount on your reservations',
}];

@Component({
  selector: 'app-discount-alarm',
  templateUrl: './discount-alarm.component.html',
  styleUrls: ['./discount-alarm.component.css']
})
export class DiscountAlarmComponent implements OnInit {
  private _success = new Subject<string>();
  alert;

  staticAlertClosed = false;
  successMessage: string;

  ngOnInit(): void {
    this.alert = ALERTS[0];
    setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    this._success.pipe(
      debounceTime(5000)
    ).subscribe(() => this.successMessage = null);
  }

}
