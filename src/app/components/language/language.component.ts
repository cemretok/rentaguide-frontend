import { Component, OnInit } from '@angular/core';
import { Language } from '../../data/language';
import { LanguageService } from '../../services/language.service';
import { of } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';
import { IndexSearchService } from '../../services/indexSearch.service';

@Component({
  selector: 'app-language',
  templateUrl: './language.component.html',
  styleUrls: ['./language.component.css']
})
export class LanguageComponent implements OnInit {
  languageList: Language[];
  languageSelected;
  indexSearch;

  constructor(public languageService: LanguageService, private indexSearchService: IndexSearchService) { }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);

    this.languageService.getLanguages().pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe(object => {
        this.languageList = object as Language[];
      });
  }

  onLanguageChange(){
    const checkedLanguages = this.languageList.filter((languages) => languages.checked);
    this.indexSearch.lang = checkedLanguages.map(x => new Language(x.id));
    localStorage.setItem('indexSearch', JSON.stringify(this.indexSearch));
  }

}
