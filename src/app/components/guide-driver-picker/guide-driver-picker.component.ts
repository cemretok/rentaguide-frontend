import { Component, OnInit } from '@angular/core';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-guide-driver-picker',
  templateUrl: './guide-driver-picker.component.html',
  styleUrls: ['./guide-driver-picker.component.css']
})
export class GuideDriverPickerComponent implements OnInit {

  indexSearch: indexSearchI;
  driverSelected;

  constructor(private indexSearchService: IndexSearchService) { }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);

    if (this.indexSearch.driverLicence.length > 0)
      this.driverSelected = this.indexSearch.driverLicence == '1' ? true : false;
  }

  onDriverChange(e) {
    this.indexSearch.driverLicence = e.target.checked == true ? '1' : '0';
    localStorage.setItem('indexSearch', JSON.stringify(this.indexSearch));
  }

}
