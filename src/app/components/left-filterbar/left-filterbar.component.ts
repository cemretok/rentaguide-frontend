import { Component, OnInit } from '@angular/core';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';
import { TranslateService } from '../../services/translate.service';

@Component({
  selector: 'app-left-filterbar',
  templateUrl: './left-filterbar.component.html',
  styleUrls: ['./left-filterbar.component.css']
})
export class LeftFilterbarComponent implements OnInit {
  indexSearch : indexSearchI;
  constructor( private indexSearchService: IndexSearchService, public translate: TranslateService) { }

  ngOnInit() {
    
  }

  search(){ 
    this.indexSearch = <indexSearchI>JSON.parse(localStorage.getItem('indexSearch'))
    this.indexSearchService.changeMessage(this.indexSearch);
  }

}
