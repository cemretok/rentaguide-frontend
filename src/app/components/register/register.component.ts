import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { ValidationService } from '../../services/validation.service';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  submitted;

  public show_dialog : boolean = false;
  public button_name : any = '';
  registerForm: any;
  indexSearch: indexSearchI;
  buttonDisabled = false;
  checkin;
  checkout;
  //submitted = false;
 // buttonDisabled = true;

  constructor(private indexSearchService: IndexSearchService, private formBuilder: FormBuilder) { }


/*
  onChange() {
    this.submitted;
    // CHANGE THE TEXT OF THE BUTTON.
    if (this.button_name =  "guideRegister")
    this.show_dialog = !this.show_dialog;
    else
    this.show_dialog= this.show_dialog;
  }
  */


ngOnInit() {
  this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);


  this.checkin = this.indexSearch.checkin;
  this.checkout = this.indexSearch.checkout;

  this.registerForm = this.formBuilder.group({
    name: ['', [Validators.required, ValidationService.holderValidator]],
    l_name: ['', [Validators.required, ValidationService.holderValidator]],
    email: ['', [Validators.required, ValidationService.emailValidator]],
    phone_first: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(11)]],
    password: ['', [Validators.required, Validators.minLength(6)]],
  });
}

get f() { return this.registerForm.controls; }
/*
onSubmit() {
  this.submitted = true;
  if (this.buttonDisabled == true) {
    return;
  } else if (this.buttonDisabled == false){
    return;
  }
}*/

onSubmit() {
  this.submitted = true;

  if (this.registerForm.invalid) {
    this.buttonDisabled = false;
    return;
  } else {
    this.buttonDisabled = true;
  }
/*
    this.checkoutService.addReservation(this.guide.id, this.indexSearch.user.id, this.indexSearch.place.id, this.indexSearch.aim.id, total_price, "Ödendi", "", this.indexSearch.checkin, this.indexSearch.checkout).pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe(object => {
        this.reservation = object;

        if (this.reservation.reservationStatus == "Success") {
          this.buttonDisabled = true;
          this.navigationSuccess();
        } else if (this.reservation.reservationStatus == "Fail") {
          this.buttonDisabled = false;
        }
      });
  }
  */


}



}
