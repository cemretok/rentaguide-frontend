import { Component, OnInit } from '@angular/core';
import { ResultBarService } from '../../services/right-filter-results.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-right-filter-results',
  templateUrl: './right-filter-results.component.html',
  styleUrls: ['./right-filter-results.component.css']
})
export class RightFilterResultsComponent implements OnInit {
  page = 1;
  pageSize = 10;
  guides = [];
  indexSearch: indexSearchI;

  constructor(private resultBarService: ResultBarService, private router: Router, private indexSearchService: IndexSearchService) {
    this.indexSearchService.currentMessage.subscribe(value => {
      this.indexSearch = value;
      this.resultBarService.getResultBar(value).pipe(
        retry(2),
        catchError(() => {
          return of({});
        })).subscribe(object => {
          if (object != null) {
            this.guides = Object.values(object);
          }
        });
    });
  }

  ngOnInit() {

}

  navigate(guide_id) {
    this.indexSearch.guide.id = guide_id;
    this.indexSearchService.changeMessage(this.indexSearch);
    this.router.navigate(['checkout']);
  }

}
