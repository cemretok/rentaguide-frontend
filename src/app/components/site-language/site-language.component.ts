import { Component, OnInit } from '@angular/core';
import { TranslateService } from '../../services/translate.service';

@Component({
  selector: 'app-site-language',
  templateUrl: './site-language.component.html',
  styleUrls: ['./site-language.component.css']
})
export class SiteLanguageComponent implements OnInit {

  constructor(
    public translate: TranslateService) { }

  ngOnInit() {
  }

  setLang(lang: string) {
    this.translate.use(lang).then(() => { });
  }
}
