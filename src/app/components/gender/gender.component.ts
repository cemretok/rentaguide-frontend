import { Component, OnInit } from '@angular/core';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-gender',
  templateUrl: './gender.component.html',
  styleUrls: ['./gender.component.css']
})
export class GenderComponent implements OnInit {
  gender: any;
  genderSelected: string;
  itemsList = ['Female', 'Male', 'Both'];
  indexSearch: indexSearchI;

  constructor(private indexSearchService: IndexSearchService) { }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);

    if (this.indexSearch.gender.length > 0) {
      this.genderSelected = this.indexSearch.gender;
    }
  }

  onItemChange(item) {
    this.gender = this.itemsList.indexOf(item);
    this.indexSearch.gender = this.gender;
    localStorage.setItem('indexSearch', JSON.stringify(this.indexSearch));
  }
}
