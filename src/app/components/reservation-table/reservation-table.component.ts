import { Component, OnInit, Pipe } from '@angular/core';
import { Reservation } from '../../data/reservation';
import { ReservationService } from '../../services/reservation.service';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';
import { constants } from '../../constants/constants';

@Component({
  selector: 'app-reservation-table',
  templateUrl: './reservation-table.component.html',
  styleUrls: ['./reservation-table.component.css']
})
export class ReservationTableComponent implements OnInit {
  reservations: Reservation;
  indexSearch: indexSearchI;
  constructor(private reservationService: ReservationService, private indexSearchService: IndexSearchService) {
    this.indexSearchService.currentMessage.subscribe(value => { this.indexSearch = value; });

    if (localStorage.getItem("userType") === constants.USER_TYPE_USER) {
      this.reservationService.getUserReservation(localStorage.getItem(constants.USER_TYPE_USER.toString())).subscribe(data => {
        this.reservations = data as Reservation;

        console.log(this.reservations);
      },
        (err) => console.error(err),
        () => { });
    } else if (localStorage.getItem("userType") === constants.USER_TYPE_GUIDE) {
      this.reservationService.getGuideReservation(localStorage.getItem(constants.USER_TYPE_GUIDE.toString())).subscribe(data => {
        this.reservations = data as Reservation;

        console.log(this.reservations);
      },
        (err) => console.error(err),
        () => { });
    } else {
      console.log("Yok");
    }

  }

  cancelReservation(reservation: Reservation) {
    console.log("cancelReservation");
    reservation.reservationStatus = "0"; //iptal
    this.reservationService.cancelReservation(reservation).subscribe(data => {
      this.reservations = data as Reservation;
      console.log(this.reservations);
    },
      (err) => console.error(err),
      () => {});
  }


  ngOnInit(): void {
  }

}
