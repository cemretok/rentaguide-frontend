import { Component, OnInit } from '@angular/core';
import { NgbDatepickerConfig, NgbDate, NgbCalendar } from '@ng-bootstrap/ng-bootstrap';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-date-picker',
  templateUrl: './date-picker.component.html',
  styleUrls: ['./date-picker.component.css']
})
export class DatePickerComponent implements OnInit {
  hoveredDate: NgbDate;

  fromDate: NgbDate = null;
  toDate: NgbDate = null;;
  indexSearch: indexSearchI;
  checkThisIn;
  checkThisOut;

  constructor(config: NgbDatepickerConfig, calendar: NgbCalendar, private indexSearchService: IndexSearchService) {
    //this.fromDate = calendar.getToday();
    //this.toDate = calendar.getNext(calendar.getToday(), 'd', 10);

    let l_now = calendar.getToday()
    let l_next = calendar.getNext(calendar.getToday(), 'y', 2);

    config.minDate = { year: l_now.year, month: l_now.month, day: l_now.day };
    config.maxDate = { year: l_next.year, month: l_next.month, day: l_next.month };
    config.outsideDays = "hidden";

    //config.markDisabled = (date: NgbDate) => calendar.getPrev(date, 'd', l_now.day).day < l_now.day;
  }

  ngOnInit() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);

    let isMessageNull = this.indexSearch.checkin.length == 0 && this.indexSearch.checkout.length == 0;

    if (!isMessageNull) {
      this.checkThisIn = this.indexSearch.checkin.split("-");
      this.checkThisOut = this.indexSearch.checkout.split("-");

      this.fromDate = new NgbDate(parseInt(this.checkThisIn[0]), parseInt(this.checkThisIn[1]), parseInt(this.checkThisIn[2]));
      this.toDate = new NgbDate(parseInt(this.checkThisOut[0]), parseInt(this.checkThisOut[1]), parseInt(this.checkThisOut[2]));
    }
  }

  onDateSelection(date: NgbDate) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
      this.toDate = date;
    } else if ((this.fromDate && this.toDate) && (this.fromDate.equals(this.toDate)) && date.after(this.fromDate)) {
      this.toDate = date;
    } else if ((this.fromDate && this.toDate) && (!this.fromDate.equals(this.toDate))) {
      this.fromDate = date;
      this.toDate = null;
    } else if (this.fromDate && !this.toDate && date.after(this.fromDate)) {
      this.toDate = date;
    } else {
      this.fromDate = null;
      this.toDate = null;
    }

    if (!(this.fromDate == null)) {
      this.indexSearch.checkin = (this.fromDate.day.toString().length > 1 ? this.fromDate.day.toString() : '0' + this.fromDate.day.toString()) + '-'
        + (this.fromDate.month.toString().length > 1 ? this.fromDate.month.toString() : '0' + this.fromDate.month.toString()) + '-'
        + this.fromDate.year.toString();
    }

    if (!(this.toDate == null)) {
      this.indexSearch.checkout = (this.toDate.day.toString().length > 1 ? this.toDate.day.toString() : '0' + this.toDate.day.toString()) + '-'
        + (this.toDate.month.toString().length > 1 ? this.toDate.month.toString() : '0' + this.toDate.month.toString()) + '-'
        + this.toDate.year.toString();
    }

    this.indexSearchService.changeMessage(this.indexSearch);
  }

  isHovered(date: NgbDate) {
    return this.fromDate && !this.toDate && this.hoveredDate && date.after(this.fromDate) && date.before(this.hoveredDate);
  }

  isInside(date: NgbDate) {
    return date.after(this.fromDate) && date.before(this.toDate);
  }

  isRange(date: NgbDate) {
    return date.equals(this.fromDate) || date.equals(this.toDate) || this.isInside(date) || this.isHovered(date);
  }

}
