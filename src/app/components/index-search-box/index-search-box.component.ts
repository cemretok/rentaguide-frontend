import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { IndexSearchService } from '../../services/indexSearch.service';
import { indexSearchI } from '../../data/indexSearch';

@Component({
  selector: 'app-index-search-box',
  templateUrl: './index-search-box.component.html',
  styleUrls: ['./index-search-box.component.css']
})
export class IndexSearchBoxComponent implements OnInit {
  indexSearch: indexSearchI;
  constructor(private router: Router, private indexSearchService: IndexSearchService) { }
  ngOnInit() { }

  navigate() {
    this.indexSearchService.currentMessage.subscribe(message => this.indexSearch = message);


    if (this.indexSearch.place.id != "0" && this.indexSearch.checkin != "" && this.indexSearch.checkout != "") {
      this.router.navigate(['search-results']);
    }
  }
}
