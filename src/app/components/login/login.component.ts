import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService } from '../../services/authentication.service';
import { constants } from '../../constants/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  password = 'Kamil'
  mail = 'kamil@gmail.com'
  userType = 'Guide'
  invalidLogin = false
  authData: any;

  constructor(private _location: Location, private router: Router,
    private loginservice: AuthenticationService) { }

  ngOnInit() {
  }

  checkLogin() {
    this.authData = this.loginservice.authenticate(this.mail, this.password, this.userType);


    console.log(this.authData);
    if (this.authData) {
      this._location.back();
      this.invalidLogin = false;

      if (this.authData.userType == constants.USER_TYPE_USER) {
        localStorage.setItem("userType", constants.USER_TYPE_USER.toString());
      } else if (this.authData.userType == constants.USER_TYPE_GUIDE) {
        localStorage.setItem("userType", constants.USER_TYPE_GUIDE.toString());
      } else if (this.authData.userType == constants.USER_TYPE_ADMIN) {
        localStorage.setItem("userType", constants.USER_TYPE_ADMIN.toString());
      } else
        this.invalidLogin = true;
    }
  }
}
