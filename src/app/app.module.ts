import { TranslateService } from './services/translate.service';
import { IndexSearchService } from './services/indexSearch.service';
import { indexSearchI } from './data/indexSearch';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppComponent } from './app.component';
import { routingComponents, AppRoutingModule } from './app-routing.module';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SuccessReservationComponent } from './components/success-reservation/success-reservation.component';
import { IndexSearchBoxComponent } from './components/index-search-box/index-search-box.component';
import { DatePickerComponent } from './components/date-picker/date-picker.component';
import { CountryInputComponent } from './components/country-input/country-input.component';
import { DiscountAlarmComponent } from './components/discount-alarm/discount-alarm.component';
import { LeftFilterbarComponent } from './components/left-filterbar/left-filterbar.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { RightFilterResultsComponent } from './components/right-filter-results/right-filter-results.component';
import { FooterComponent } from './components/footer/footer.component';
import { GenderComponent } from './components/gender/gender.component';
import { SearchResultComponent } from './components/result-list/result-list.component';
import { CheckoutComponent } from './pages/checkout/checkout.component';
import { LanguageComponent } from './components/language/language.component';
import { GuideDriverPickerComponent } from './components/guide-driver-picker/guide-driver-picker.component';
import { AdminComponent } from './pages/admin/admin.component';
import { TranslatePipe } from './pipe/translate.pipe';
import { ArraySortPipe } from './pipe/sort.pipe';
import { DateFormatPipe } from './pipe/dateFormat.pipe'
import { AdminSidebarComponent } from './components/admin-sidebar/admin-sidebar.component';
import { GuideAccountComponent } from './pages/guide-account/guide-account.component';
import { GuideAccountSidebarComponent } from './components/guide-account-sidebar/guide-account-sidebar.component';
import { UserAccountComponent } from './pages/user-account/user-account.component';
import { UserAccountSidebarComponent } from './components/user-account-sidebar/user-account-sidebar.component';
import { UserProfileComponent } from './pages/user-profile/user-profile.component';
import { UserReservationComponent } from './pages/user-reservation/user-reservation.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { LogoutComponent } from './components/logout/logout.component';
import { SiteLanguageComponent } from './components/site-language/site-language.component';
import { CountryInputService } from './services/country-input.service';
import { ValidationService } from './services/validation.service';
import { AuthenticationService } from './services/authentication.service';
import { BasicAuthHtppInterceptorService } from './services/basic-auth-htpp-interceptor.service';
import { ReservationTableComponent } from './components/reservation-table/reservation-table.component';
import { UserProfileFormComponent } from './components/user-profile-form/user-profile-form.component';


export function setupTranslateFactory(service: TranslateService): Function {
  return () => service.use('tr');
}

export function setupLocalStorageFactory(indexSearchService: IndexSearchService): Function {

  if (localStorage.getItem('indexSearch') !== null) {
    var indexData = <indexSearchI>JSON.parse(localStorage.getItem('indexSearch'))
    if (new Date(indexData.expire) > new Date(Date.now())) {
      indexSearchService.changeMessage(JSON.parse(localStorage.getItem('indexSearch')));
    } else {
      localStorage.removeItem('indexSearch');
      localStorage.setItem('indexSearch', JSON.stringify(indexSearchService.newMessage()));
    }
  } else {
    localStorage.setItem('indexSearch', JSON.stringify(indexSearchService.newMessage()));
  }
  return () => { };
}
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    PageNotFoundComponent,
    NavbarComponent,
    SuccessReservationComponent,
    IndexSearchBoxComponent,
    DatePickerComponent,
    CountryInputComponent,
    DiscountAlarmComponent,
    LeftFilterbarComponent,
    SearchBarComponent,
    RightFilterResultsComponent,
    FooterComponent,
    GenderComponent,
    SearchResultComponent,
    CheckoutComponent,
    LoginComponent,
    LanguageComponent,
    GuideDriverPickerComponent,
    RegisterComponent,
    CheckoutComponent,
    LogoutComponent,
    AdminComponent,
    SiteLanguageComponent,
    TranslatePipe,
    ArraySortPipe,
    DateFormatPipe,
    AdminSidebarComponent,
    GuideAccountComponent,
    GuideAccountSidebarComponent,
    UserAccountComponent,
    UserAccountSidebarComponent,
    UserProfileComponent,
    UserReservationComponent,
    ReservationTableComponent,
    UserProfileFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [CountryInputService, IndexSearchService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupLocalStorageFactory,
      deps: [IndexSearchService],
      multi: true
    }, ValidationService, AuthenticationService, {
      provide: HTTP_INTERCEPTORS, useClass: BasicAuthHtppInterceptorService, multi: true
    },
    TranslateService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true
    }],
  bootstrap: [AppComponent]
})
export class AppModule { }
