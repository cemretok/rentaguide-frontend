import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../data/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userSource = new BehaviorSubject<User>(this.);
  currentUser = this.userSource.asObservable();
  constructor(private http: HttpClient) { }

  getUserInfo(data){
    return this.http.post(environment.findUser, data);
  }
}
