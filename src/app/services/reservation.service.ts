import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Reservation } from '../data/reservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private http: HttpClient) { }

  getUserReservation(userId: string): Observable<Reservation> {
    return this.http.post<Reservation>(environment.getUserReservation + "/" + userId, { title: 'getUserReservation' });
  }

  getGuideReservation(guideId: string): Observable<Reservation> {
    return this.http.post<Reservation>(environment.getGuideReservation + "/" + guideId, { title: 'getGuideReservation' });
  }

  cancelReservation(reservation: Reservation): Observable<Reservation> {
    return this.http.put<Reservation>(environment.updateReservation, reservation);
  }
}
