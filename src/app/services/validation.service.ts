import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ValidationService {

  constructor() { }

  static holderValidator(control){
    if (control.value.match(/[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)){
      return null;
    }else{
      return {'InvalidHolder': true};
    }
  }

  static yearValidator(control){
    if (control.value.match(/^(1|2)\d{1}$/)){
      return null;
    }else{
      return {'InvalidYear': true};
    }
  }

  static monthValidator(control){
    if (control.value.match(/^(0?[1-9]|1[012])$/)){
      return null;
    }else{
      return {'InvalidMonth': true};
    }
  }

  /*
  static nameValidator(control){
    if (control.value.match(/[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$/)){
      return null;
    }else{
      return {'InvalidName': true};
    }
  }
*/

  static emailValidator(control){
    if (control.value.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)){
      return null;
    }else{
      return {'InvalidEmail':true};
    }

  }
}