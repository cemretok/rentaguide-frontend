import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class CheckoutService {

  constructor(private http: HttpClient) { }

  getCheckoutGuide(guide_id) {
    return this.http.post(environment.getOneGuide, guide_id);
  }

  getGuidePrice(guide_id, place_id) {
    return this.http.post(environment.getOneGuidePrice, {
      "guide": {
        "id": guide_id
      },
      "place": {
        "id": place_id
      }
    });
  }

  addReservation(guide_id, user_id, place_id, aim_id, total_price, status, notes, checkin, checkout) { 
    console.log("notes: " + notes);
    return this.http.post(environment.addReservation, {
      "guide": {
        "id": guide_id
      },
      "user": {
        "id": user_id
      },
      "place": {
        "id": place_id
      },
      "aim": {
        "id": aim_id
      },
      "totalPrice": total_price,
      "reservationStartDate": checkin + " 00:00:00",
      "reservationEndDate": checkout + " 00:00:00",
      "status": status,
      "notes": notes
    });
  }
}