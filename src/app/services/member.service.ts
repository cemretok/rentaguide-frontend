import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { GuideAccountComponent } from '../pages/guide-account/guide-account.component';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  constructor(private http: HttpClient) { }


  getGuideInfo(data){
    return this.http.post(environment.findGuide, data);
  }

  getUserInfo(data){
    return this.http.post(environment.findUser, data);
  }
}
