import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators';
import { of } from 'rxjs';
import { environment } from '../../environments/environment';
import { MemberService } from '../services/member.service';
import { constants } from '../constants/constants';
import { Guide } from '../data/guide';
import { User } from '../data/user';

export class JwtResponse {
  constructor(
    public jwttoken: string,
  ) { }

}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient, private memberService: MemberService) { }
  userData = <User>{};
  guideData = <Guide>{};
  authData: any;



  authenticate(mail, password, userType) {
    let httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Type': userType }),
      observe: 'response' as 'response'
    };

    return this.http.post<any>(environment.userLogin, {
      "mail": mail,
      "password": password
    }, httpOptions).pipe(
      retry(2),
      catchError(() => {
        return of({});
      })).subscribe((data: HttpResponse<any>) => {

        this.authData = data;

        if (userType == constants.USER_TYPE_GUIDE) {
          this.guideData.mail = mail;

          this.memberService.getGuideInfo(this.guideData).subscribe((data: HttpResponse<Guide>) => {
            localStorage.setItem('guide', JSON.stringify(data));
            localStorage.setItem('userType', constants.USER_TYPE_GUIDE.toString());
            this.authData.userType = constants.USER_TYPE_GUIDE
          });
        } else if (userType == constants.USER_TYPE_USER) {
          this.userData.mail = mail;

          this.memberService.getUserInfo(this.userData).subscribe((data: HttpResponse<User>) => {
            console.log(data);
            localStorage.setItem('user', JSON.stringify(data));
            localStorage.setItem('userType', constants.USER_TYPE_USER.toString());
            this.authData.userType = constants.USER_TYPE_USER
          });
        }

        sessionStorage.setItem('mail', mail);
        sessionStorage.setItem('token', data.headers.get('Authorization'));
        return this.authData;
      }
      );
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('mail')
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('mail')
  }
}