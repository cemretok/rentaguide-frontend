import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ResultBarService {

  constructor(private http: HttpClient) { }

  getResultBar(data){
    return this.http.post(environment.getResultBar, data);
  }
}
