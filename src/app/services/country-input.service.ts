import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CountryInputService {

  constructor(private http: HttpClient) { }

  getCountry(){
    return this.http.get(environment.getCountry);
  }
}
