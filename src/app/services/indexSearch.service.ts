import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { indexSearchI } from '../data/indexSearch';

@Injectable()
export class IndexSearchService {

  indexSearch: indexSearchI = {
    user: {
      id: '0'
    },
    guide: {
      id: '0'
    },
    place: {
      id: '0',
      country: '',
      province: '',
      district: ''
    },
    checkin: '',
    checkout: '',
    gender: '2', //2 both 1 male 0 female
    driverLicence: '0',
    lang: [{ id: '1' }],
    aim: {
      id: '0',
      aim: null,
      aimDetail: null
    },
    minPrice: null,
    maxPrice: null,
    currency: null,
    expire: null
  };


  private messageSource = new BehaviorSubject<indexSearchI>(this.indexSearch);
  currentMessage = this.messageSource.asObservable();

  constructor() { }

  changeMessage(message: indexSearchI) {
    message.expire = new Date(Date.now() + 1000 * 20); //20 Dakika sonra expire ol
    this.messageSource.next(message);
    localStorage.setItem('indexSearch', JSON.stringify(message));
  }

  newMessage() {
    this.changeMessage(this.indexSearch);

    return this.indexSearch;
  }

}
