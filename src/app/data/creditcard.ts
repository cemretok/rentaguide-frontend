export class CreditCard {
    holder: string;
    month: string;
    year: string;
    ccn: string;
    cvc: string;

    constructor(holder, month, year, ccn, cvc){
        this.holder = holder;
        this.month = month;
        this.year = year;
        this.ccn = ccn;
        this.cvc = cvc;
    }
  }