export interface Guide {
    id: number;
    name: String;
    surname: String;
    mail: String;
    password: String;
    gender: String;
    profile: String;
    profilePicPath: String;
    phone: String;
    registerDate: Date;
    driverLicence: String;
}