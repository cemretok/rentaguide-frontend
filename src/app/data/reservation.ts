import { Guide } from './guide';
import { User } from './user';
import { Aim } from './aim';
import { Place } from './place';

export interface Reservation {
    id: number;
    guide: Guide;
    user: User;
    place: Place;
    aim: Aim;
    reservationType: String;
    reservationStartDate: Date;
    reservationEndDate: Date;
    totalPrice: number;
    reservationStatus: String;
    notes: String;
}