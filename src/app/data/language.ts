export class Language {
    id: number;
    name: string;
    checked?: boolean;

    constructor(id?: number, name?: string, checked?: undefined) {
        if (id && name && checked) {
            this.id = id;
            this.name = name;
            this.checked = false;
        }

        if (id && !name && !checked) {
            this.id = id;
        }
    }
}
