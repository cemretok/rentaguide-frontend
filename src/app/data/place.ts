export interface Place {
    id: number;
    country: String;
    province: String;
    district: String;
}