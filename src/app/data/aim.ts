export interface Aim {
    id: number;
    aim: String;
    aimDetail: String;
}