
export interface indexSearchI {
    user: {
        id: string
    },
    guide: {
        id: string
    },
    place: {
        id: string;
        country: string;
        province: string;
        district: string;
    },
    checkin: string;
    checkout: string;
    gender: string;
    driverLicence: string;
    lang: any;
    aim: {
        id: string,
        aim: string,
        aimDetail: string
    },
    minPrice: string,
    maxPrice: string,
    currency: string,
    expire: Date
}


