export interface User {
    id: number;
    name: String;
    surname: String;
    password: String;
    mail: String;
    phone: String;
    registerDate: Date
    passportNo: String;
}