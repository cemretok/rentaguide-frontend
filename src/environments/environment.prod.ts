export const environment = {
  production: true,
  getGuide: '/guide/getall',
  findUser: '/user/find',
  findGuide: '/guide/find',
  getCountry: '/place/getall',
  getLanguage: '/language/getall',
  getOneGuide: '/guide/getone',
  getResultBar: '/indexsearch/find',
  getSearchResult: '/place/getall',
  getOneGuidePrice: '/guideprice/getone',
  makePayment: '/payment/pay',
  getUserReservation: '/reservation/getUser',
  getGuideReservation: '/reservation/getGuide',
  addReservation: '/reservation/save',
  updateReservation: '/reservation/update',
  userLogin: '/login'
};
