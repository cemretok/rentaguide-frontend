// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  getGuide: 'http://www.mocky.io/v2/5cbe08a82f0000ac0c16cfdc',  
  findUser: 'http://www.mocky.io/v2/5cbe08a82f0000ac0c16cfdc',
  findGuide: 'http://www.mocky.io/v2/5cbe08a82f0000ac0c16cfdc',
  getCountry: 'http://www.mocky.io/v2/5cb8c9a04c0000511bd3d758',
  getLanguage: 'http://www.mocky.io/v2/5cb8c9a04c0000511bd3d758',
  getOneGuide: 'http://www.mocky.io/v2/5cbe08a82f0000ac0c16cfdc',
  getResultBar: 'http://www.mocky.io/v2/5cb4a23e3300001b4611bc5f',
  getSearchResult: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5',
  getOneGuidePrice: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5',
  makePayment: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5',
  getUserReservation: 'http://www.mocky.io/v2/5e53c9062e000065002daf0a',
  getGuideReservation: 'http://www.mocky.io/v2/5e53c9062e000065002daf0a',
  addReservation: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5',
  updateReservation: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5',
  userLogin: 'http://www.mocky.io/v2/5ca4cea433000050002ea4f5'
};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
